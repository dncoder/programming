import string
def caeser( p_char, shift ):
	p_char = p_char.upper()
	l_ind = string.ascii_uppercase.index(p_char)
	c_ind = (l_ind + shift) % len(string.ascii_uppercase)
	c_char = string.ascii_uppercase[c_ind]
	return c_char
p_text = "hello world"
shift = 7
c_array = [caeser( char, shift ) for char in p_text if char in string.ascii_letters]
print( "".join(c_array) )
