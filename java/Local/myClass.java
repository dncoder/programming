
import java.util.Scanner;

class myClass{
	public static void main( String args[] ){
		int[] num = new int[10];
		int i;
		Scanner in = new Scanner( System.in );
		for( i=0; i<10; i++ ){
			System.out.printf( "Enter Value %d :", i );
			num[i] = in.nextInt();
		}
		for( int n : num ){
			System.out.printf( "%3d :", n );
			for( i=0; i<n; i++ )
				System.out.printf("##");
			System.out.println();
		}
	}
}
class myClassNew{
	public static void main( String args[] ){
		int[] num = new int[10];
		int i;
		Scanner in = new Scanner( System.in );
		for( i=0; i<10; i++ ){
			System.out.printf( "Enter Value %d :", i );
			num[i] = in.nextInt();
		}
		for( int n : num ){
			System.out.printf( "%3d :", n );
			for( i=0; i<n; i++ )
				System.out.printf("##");
			System.out.println();
		}
	}
}
