// Sin Cos Tan
import java.util.Scanner;
import java.lang.Math;
class CubeSquare{

	private int num;

	public void accept(){
		Scanner scn = new Scanner( System.in );
		System.out.println("Enter Num :");
		num = scn.nextInt();
	}

	public void display(){
		System.out.println( "Square Value = " + Math.pow( num, 2 ) );
		System.out.println( "Cube Value= " + Math.pow( num, 3 ) );
	}
}

class Main{
	public static void main( String args[] ){
		CubeSquare obj = new CubeSquare();
		obj.accept();
		obj.display();
	}
}
