// Sin Cos Tan
import java.util.Scanner;
import java.lang.Math;
class Trigono{

	private int angle;
	private double sin;
	private double cos;
	private double tan;

	public void accept(){
		Scanner scn = new Scanner( System.in );
		System.out.println("Enter Angle :");
		angle = scn.nextInt();
	}

	public void calculate(){
		sin = Math.sin( angle );
		cos = Math.cos( angle );
		tan = Math.tan( angle );
	}

	public void display(){
		System.out.println( "Sin Value = " + sin );
		System.out.println( "Cos Value= " + cos );
		System.out.println( "Tan Value= " + tan );
	}
}

class Main{
	public static void main( String args[] ){
		Trigono obj = new Trigono();
		obj.accept();
		obj.calculate();
		obj.display();
	}
}
