// Sin Cos Tan
import java.util.Scanner;
import java.lang.Math;
class Temprature{

	public int[] temprature = new int[7];

	public void accept(){
		Scanner scn = new Scanner( System.in );
		int i;
		for( i=0; i<7; i++ ){
			System.out.println("Enter Temprature " + i);
			temprature[i] = scn.nextInt();
		}
	}
}

class Main{
	public static void main( String args[] ){
		Temprature obj = new Temprature();
		obj.accept();
		int i, sum=0;
		double avg;
		for( i=0; i<7; i++ )
			sum += obj.temprature[i];
		avg = sum/7;
		System.out.println( "Average temprature = " + avg );

	}
}
