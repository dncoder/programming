// Area of square rectangle and trapezium
import java.util.Scanner;
class OverloadArea{
	
	void area( int side ){
		int area = side*side;
		System.out.println( "Area of Square = " + area );
	}
	void area( int length, int breadth ){
		int area = length*breadth;
		System.out.println( "Area of Rectangle = " + area );
	}
	void area( int base1, int base2, int height ){
		int area = height * ( (base1 + base2)/2 );
		System.out.println( "Area of Trapezium = " + area );
	}

}

class Main{
	public static void main( String args[] ){
		int side, length, breadth, base1, base2, height, ch;
		OverloadArea area = new OverloadArea();
		Scanner scn = new Scanner( System.in );
		System.out.println( "AREA CALCULATION MENU" );
		System.out.println( " 1. AREA OF SQUARE" );
		System.out.println( " 2. AREA OF RECTANGLE" );
		System.out.println( " 3. AREA OF TRAPEZIUM" );
		System.out.print( " Enter Your Choice : " );
		ch = scn.nextInt();
		switch( ch ){
			case 1:
				System.out.print( "Enter Side of Square : " );
				side = scn.nextInt();
				area.area( side );
				break;
			case 2:
				System.out.print( "Enter Length Rectangle : " );
				length = scn.nextInt();
				System.out.print( "Enter Breadth Rectangle : " );
				breadth = scn.nextInt();
				area.area( length, breadth );
				break;
			case 3:
				System.out.print( "Enter Base 1 Trapezium : " );
				base1 = scn.nextInt();
				System.out.print( "Enter Base 2 Trapezium : " );
				base2 = scn.nextInt();
				System.out.print( "Enter height Trapezium : " );
				height = scn.nextInt();
				area.area( base1, base2, height );
				break;
			default:
				System.out.println("Wrong Choice");
				break;
		}
	}
}
