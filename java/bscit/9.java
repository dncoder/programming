//VOLUME OF CYLINDER
import java.util.Scanner;
import java.lang.Math;
class BaseClass1{
	int r;
	void acceptRadius(){
		Scanner scn = new Scanner( System.in );
		System.out.println( "Enter Radius : " );
		r = scn.nextInt();
	}
}

class BaseClass2 extends BaseClass1{
	int h;
	void acceptHeight(){
		Scanner scn = new Scanner( System.in );
		System.out.println( "Enter Height : " );
		h = scn.nextInt();
	}
}

class DerivedClass extends BaseClass2{
	double v;
	void volume(){
		v = Math.PI * r * r * h;
		System.out.println( "Volume of Cylinder = " + v );
	}
}

class Main{
	public static void main( String args[] ){
		DerivedClass cyl = new DerivedClass();
		cyl.acceptRadius();
		cyl.acceptHeight();
		cyl.volume();
	}
}
