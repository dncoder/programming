//CONSTRUCTOR
class InitConstruct{
	int a;
	int b;
	InitConstruct(){
		a=5;
		b=10;
	}
	void display(){
		System.out.println( "Values of A = " + a );
		System.out.println( "Values of B = " + b );
	}
}

class Main{
	public static void main( String args[] ){
		InitConstruct ab = new InitConstruct();
		ab.display();
	}
}
