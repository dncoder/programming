//CONSTRUCTOR
import java.util.Scanner;
class BaseClass{
	int a;
	int b;
	void accept(){
		Scanner scn = new Scanner( System.in );
		System.out.println( "Enter Value of A : " );
		a = scn.nextInt();
		System.out.println( "Enter Value of B : " );
		b = scn.nextInt();
	}
}

class DerivedClass extends BaseClass{
	int c;
	void add(){
		c = a+b;
		System.out.println( "Addition = " + c );
	}
	void subtract(){
		c = a-b;
		System.out.println( "Subtraction = " + c );
	}
	void multiply(){
		c = a*b;
		System.out.println( "Multiplication = " + c );
	}
	void divide(){
		c = a/b;
		System.out.println( "Division = " + c );
	}
}

class Main{
	public static void main( String args[] ){
		DerivedClass ab = new DerivedClass();
		ab.accept();
		ab.add();
		ab.subtract();
		ab.multiply();
		ab.divide();
	}
}
