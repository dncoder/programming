//Palindrome or not
import java.util.Scanner;

class palindrome{
	public static void main( String args[] ){
		Scanner scn = new Scanner( System.in );
		String str, rev="" ;
		int i,l;
		System.out.println( "Enter a word : " );
		str = scn.nextLine();
		l = str.length();
		for( i=l-1; i>=0; i-- )
			rev += str.charAt(i);
		if( rev.equals(str) )
			System.out.println( str + " Is Palindrome");
		else
			System.out.println( str + " Is Not Palindrome");
	}
}
