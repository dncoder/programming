import java.io.*;
class FileHandle{
	public static void main( String args[] ) throws IOException{
		File fo = new File("test.txt");
		if( !fo.exists() ){
			System.out.println( "File Not Found, Creating" );
			fo.createNewFile();
		}
		FileWriter fw = new FileWriter( fo );
		System.out.println( "Writing Data Into The File" );
		fw.write( "This is a text\n" );
		fw.flush();
		fw.close();
		FileReader fr = new FileReader( fo );
		char ch[] = new char[100];
		System.out.println( "Reading Data From The File" );
		fr.read( ch );
		String text = "";
		for( char c : ch )
			text += c;
		System.out.printf( "data : %s\n", text );
	}
}
