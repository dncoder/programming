//Total And Average of Student in 3 Subjects

#include<iostream>

using namespace std;

int main(){
	int sub1, sub2, sub3, tot;
	double avg;
	cout << "Enter Marks of 3 Subjects \n";
	cin >> sub1 >> sub2 >> sub3;
	tot = sub1 + sub2 + sub3;
	avg = tot/3;
	cout << "Total = " << tot << endl << "Average = " << avg << endl;
	return 0;
}
