//Find The Third Angle of a triangle if 2 are given

#include<iostream>

using namespace std;

int main(){
	double ang1, ang2, ang3;
	cout << "Enter the 2 Angles of a triangle \n";
	cin >> ang1 >> ang2;
	ang3 = 180 - (ang1+ang2);
	cout << "Third Angle = " << ang3 << endl;
	return 0;
}
