//Total Resistance When Connected in series and parallel

#include<iostream>

using namespace std;

int main(){
	double res1, res2, res_pr, res_sr;
	cout << "Enter the 2 Resistance \n";
	cin >> res1 >> res2;
	res_sr = res1 + res2;
	res_pr = (1/res1) + (1/res2);
	cout << "Series Total = " << res_sr << endl;
	cout << "Parallel Total = " << res_pr << endl;
	return 0;
}
