//Meter into Centimeter

#include<iostream>

using namespace std;

int main(){
	double m, cm;
	cout << "Enter Length in Meters \n";
	cin >> m;
	cm = m*100;
	cout << "Length in Centimeter = " << cm << endl;
	return 0;
}
