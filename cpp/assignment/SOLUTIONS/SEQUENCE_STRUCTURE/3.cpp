//CALCULATE AREA OF A CIRCLE

#include<iostream>
#include<math.h>

using namespace std;

int main(){
	int rd;
	double area;
	cout << "Enter Radius of circle \n";
	cin >> rd;
	area = M_PI * rd * rd;
	cout << "Area of Circle = " << area;
	return 0;
}
