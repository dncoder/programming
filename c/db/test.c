#include<stdio.h>
#include<stdlib.h>

typedef struct student{
	int roll;
	char name[100];
	char course[100];
} student;

void addData(){
	FILE *fl = fopen("student.data", "bw+");
	student stud;
	char ch='n';
	while(ch!='y'){
		system("clear");
		printf("Enter Name :\n");
		fgetc(stdin);
		fgets(stud.name, sizeof(stud.name), stdin);
		printf("Enter Course :\n");
		fgetc(stdin);
		fgets(stud.course, 100, stdin);
		printf("Enter Roll :\n");
		scanf("%d", &stud.roll);
		fwrite( &stud, 1, sizeof(stud), fl );
		printf("Data Inserted Successfully\nInsert More? [y/n] ");
		scanf("%c", &ch);
	}
}

int main(){
	student std;
	int ch;
	while( ch!=3 ){
		system("clear");
		printf("%-15s\n%-15s\n%-15s\n%-15s",
				"Menu",
				"1. View Data",
				"2. Add Data",
				"3. Exit");
		printf("\nEnter Your Choice : ");
		scanf("%d", &ch);
		switch( ch ){
			case 2:
				addData();
		}
	}
	system("clear");
	return 0;
}
