#include<stdio.h>
#include<string.h>

void main(){
	int old=0, new=1, now, i=0, t;
	printf("Enter Number Of Terms : ");
	scanf("%d", &t);
	while(i<t){
		now = old+new;
		printf( "%d ", now );
		old = new;
		new = now;
		i++;
	}
	printf("\n");
}
