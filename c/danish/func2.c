#include<stdio.h>
#include<math.h>

double getAnswer( int b, int h ){
	double area = 0.5*b*h;
	return(area);
}

void main(){
	int b,h;
	printf( "Enter base of trangle : " );
	scanf( "%d", &b );
	printf( "Enter height of trangle : " );
	scanf( "%d", &h );
	double area = getAnswer( b, h );
	printf( "Area = %lf", area );
}

