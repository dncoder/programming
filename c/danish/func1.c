#include<stdio.h>
#include<math.h>

int getAnswer( int a, int b ){
		int ans = pow(a,2) + pow(b,2) + 2*a*b;
		return(ans);
}

void main(){
	int a,b;
	printf("Enter value of a :");
	scanf( "%d", &a );
	printf("Enter value of b :");
	scanf( "%d", &b );
	int ans = getAnswer(a,b);
	printf("%d", ans);
}

