#include<stdio.h>

typedef struct date{
	char dob[100];
	char doj[100];
} date;

typedef struct Employee{
	int empid;
	char empname[100];
	date empdate;
} Employee;

void main(){
	Employee emp;
	printf( "Enter Employee Name: " );
	scanf( "%s", emp.empname );
	printf( "Enter Employee ID: " );
	scanf( "%d", &emp.empid );
	printf( "Enter Employees Date Of Birth in dd/mm/yy format: " );
	scanf( "%s", emp.empdate.dob );
	printf( "Enter Employees Date Of Joing in dd/mm/yy format: " );
	scanf( "%s", emp.empdate.doj );

	//Printing Out Details
	printf( "Employees Data\n" );
	printf( "\nID\t%d", emp.empid );
	printf( "\nNAME\t%s", emp.empname );
	printf( "\nDate Of Birth \t%s", emp.empdate.dob );
	printf( "\nDate Of Joining \t%s", emp.empdate.doj );
}

