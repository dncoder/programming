using System;
namespace Danish{
	class Hello{
		public static void Main(){
			Student s = new Student();
			s.SetName();
			s.SetCourse();
			s.PrintName();
			s.PrintCourse();
		}
	}
	
	class Student{
		private string name;
		private string course;
		public void SetName(){
			Console.WriteLine( "Enter your name" );
			string n = Console.ReadLine();
			name = n;
		}
		public void SetCourse(){
			Console.WriteLine( "Enter your Course" );
			string c = Console.ReadLine();
			course = c;
		}
		public void PrintName(){
			Console.WriteLine( "Name = " + name );
		}
		public void PrintCourse(){
			Console.WriteLine( "Course = " + course );
		}
	}
}
